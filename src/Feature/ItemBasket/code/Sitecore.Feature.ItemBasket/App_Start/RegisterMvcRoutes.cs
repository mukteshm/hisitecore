﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Sitecore.Pipelines;

namespace Sitecore.Feature.ItemBasket
{
    public class RegisterMvcRoutes
    {
        public virtual void Process(PipelineArgs args)
        {
            this.RegisterRoutes(RouteTable.Routes);
        }

        public bool MvcIgnoreHomePage { get; set; }

        protected virtual void RegisterRoutes(RouteCollection routes)
        {
            if (this.MvcIgnoreHomePage)
            {
                routes.IgnoreRoute(string.Empty);
            }

            RouteTable.Routes.MapRoute("ItemBasket", "ItemBasket", new { controller = "ItemBasket", action = "ItemBasket" });
            RouteTable.Routes.MapRoute("Operations", "Operations", new { controller = "ItemBasket", action = "Operations" });

            RouteTable.Routes.MapRoute("Publish", "Publish", new { controller = "Publish", action = "GetPublishDetails" });
        }
    }
}