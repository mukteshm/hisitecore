﻿using Sitecore.Shell.Framework.Commands;
using System;
using System.Linq;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using Sitecore.Shell.Applications.Dialogs.ProgressBoxes;
using Sitecore.Feature.ItemBasket.Extensions.Helper;
using Sitecore.Feature.ItemBasket.Extensions.Sitecore;
using Sitecore.Buckets.Managers;
using Sitecore.Feature.ItemBasket.Repository;

namespace Sitecore.Feature.ItemBasket.Commands
{
    public class AddToBasket : Command
    {
        private readonly ItemBasketRepository itemBasketRepository = new ItemBasketRepository();
        public override void Execute(CommandContext context)
        {
            if (context.Items.Length != 1)
            {
                return;
            }
            //check if item already exists
            var item = context.Items[0];
            var siteSettings = this.itemBasketRepository.GetSiteSettings(item);

            if (siteSettings == null)
            {
                Context.ClientPage.ClientResponse.Alert("Some Settings missing for this site.");
                return;
            }

            var itemBasketFolder = this.itemBasketRepository.GetBasketBucketItem(item);

            //check if item basket folder exists; if not create.
            if (itemBasketFolder == null)
            {
                var templateItem = item.Database.GetTemplate(Constants.TemplateIds.ItemBasketTemplateId);
                itemBasketFolder = siteSettings.Add("Item Basket", templateItem);

                BucketManager.CreateBucket(itemBasketFolder);
                BucketManager.Sync(itemBasketFolder);
            }

            var itemExists = this.itemBasketRepository.CheckifItemExistsInBasket(itemBasketFolder, item);
            if (itemExists)
            {
                Context.ClientPage.ClientResponse.Alert("Item already exists in basket");
                return;
            }

            ProgressBox.Execute("AddToBasket", "Adding to Basket", this.ExecuteOperation, context.Items[0]);

            Context.ClientPage.ClientResponse.Alert("Item added to Basket");
            Context.ClientPage.SendMessage(this, $"item:load(id={context.Items[0].ID})");
        }

        protected void ExecuteOperation(params object[] parameters)
        {
            var item = (Item)parameters[0];
            var siteSettings = this.itemBasketRepository.GetSiteSettings(item);

            if(siteSettings == null) return;
            
            var itemBasketFolder = this.itemBasketRepository.GetBasketBucketItem(item);
            
            using (new SecurityDisabler())
            {
                var masterDb = Helper.GetDatabase();

                var templateItem = masterDb.GetTemplate(Constants.TemplateIds.ItemTemplateId);
                var itemname = "Item - " + item.ID.ToShortID();
                var itemCreated = itemBasketFolder?.Add(itemname, templateItem);

                var itemToUpdate = itemCreated.Cast<_Item_Detail>();

                itemCreated?.Editing.BeginEdit();
                try
                {
                    itemToUpdate.Item_ID = item.ID.ToString();
                    itemToUpdate.Item_Path = item.Paths.Path;
                    itemToUpdate.Author = Context.User.Name;
                    itemToUpdate.Date = DateTime.Now.Date;

                    Helper.GetSitecoreService().Save(itemToUpdate);
                }
                finally
                {
                    item.Editing.EndEdit();
                    Diagnostics.Log.Info($"Item - {itemCreated?.ID} added to basket", "AddToBasket");
                }
            }
        }

        public override CommandState QueryState(CommandContext context)
        {
            if (context.Items.Length != 1) return CommandState.Hidden;
            var item = context.Items[0];

            var publishBasketFolder = this.itemBasketRepository.GetBasketBucketItem(item);

            //bucket fetching

            if (publishBasketFolder == null) return CommandState.Enabled;
            var itemExists = this.itemBasketRepository.CheckifItemExistsInBasket(publishBasketFolder, item);

            return itemExists? CommandState.Disabled : CommandState.Enabled;
        }
    }
}