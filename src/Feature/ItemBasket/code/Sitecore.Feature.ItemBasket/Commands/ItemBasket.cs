﻿using System;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Diagnostics;

namespace Sitecore.Feature.ItemBasket.Commands
{
    using Sitecore.Web.UI.Sheer;

    [Serializable]
    public class ItemBasket : Command
    {
        public override void Execute(CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            SheerResponse.ShowModalDialog(new ModalDialogOptions($"/ItemBasket?itemId={context.Items[0].ID.ToString()}") { Header = "Item Basket", Height = "500px", Width = "800px", Message = "Select the items to publish, reset layout details or recycle"});
        }
    }
}