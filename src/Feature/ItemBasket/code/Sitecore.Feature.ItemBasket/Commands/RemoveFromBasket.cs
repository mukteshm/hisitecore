﻿using System;
using Sitecore.Data.Items;
using Sitecore.Shell.Applications.Dialogs.ProgressBoxes;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Feature.ItemBasket.Repository;

namespace Sitecore.Feature.ItemBasket.Commands
{
    public class RemoveFromBasket : Command
    {
        private readonly ItemBasketRepository itemBasketRepository = new ItemBasketRepository();

        public override void Execute(CommandContext context)
        {
            if (context.Items.Length != 1)
            {
                return;
            }
            ProgressBox.Execute("RemoveFromBasket", "Remove From Basket", this.ExecuteOperation, context.Items[0]);
            Context.ClientPage.ClientResponse.Alert("Item removed from basket");
            Context.ClientPage.SendMessage(this, $"item:load(id={context.Items[0].ID})");
        }

        protected void ExecuteOperation(params object[] parameters)
        {
            var item = (Item)parameters[0];
            var siteSettings = this.itemBasketRepository.GetSiteSettings(item);
            
            if(siteSettings == null) return;

            var itemBasketFolder = this.itemBasketRepository.GetBasketBucketItem(item);

            if (itemBasketFolder == null)
            {
                throw new Exception("Item Basket folder is missing!");
            }

            if (this.itemBasketRepository.CheckifItemExistsInBasket(itemBasketFolder, item))
            {
                this.itemBasketRepository.RemoveItemFromBucket(itemBasketFolder, item);
            }
        }

        public override CommandState QueryState(CommandContext context)
        {
            if (context.Items.Length != 1) return CommandState.Hidden;

            var item = context.Items[0];
            var itemBasketFolder = this.itemBasketRepository.GetBasketBucketItem(item);

            //bucket fetching
            if (itemBasketFolder == null) return CommandState.Disabled;
            var itemExists = this.itemBasketRepository.CheckifItemExistsInBasket(itemBasketFolder, item);

            return itemExists ? CommandState.Enabled : CommandState.Disabled;
        }
    }
}