﻿namespace Sitecore.Feature.ItemBasket
{
    public static class Constants
    {
        public static class TemplateIds
        {
            public const string SiteRootTempalteId = "{0643D9D4-F30B-4B07-91D0-289CC324C9CF}";
            public const string SiteSettingTempalteId = "{4C82B6DD-FE7C-4144-BCB3-F21B4080568F}";
            public const string ItemBasketTemplateId = "{F2D51369-6D52-494D-969A-31FC3070ED49}";
            public const string ItemTemplateId = "{1E0592F7-4602-4320-9CDE-0A0BDA4EAF49}";
        }

        public const string IdCollSessionName = "ibIDCollection";
        public static class Databases
        {
            public const string MasterDatabase = "sitecore_master_index";
        }
    }
}