﻿using System.Linq;
using System.Web.Mvc;
using Sitecore.Feature.ItemBasket.Models;
using Sitecore.Data;
using Sitecore.Feature.ItemBasket.Extensions.Helper;
using Sitecore.Feature.ItemBasket.Repository;

namespace Sitecore.Feature.ItemBasket.Controllers
{
    using System;
    using Sitecore.Collections;
    using Sitecore.Data.Items;

    public class ItemBasketController : Controller
    {
        private readonly IItemBasketRepository itemBasketRepository;
        private readonly Database database;

        public ItemBasketController():this(new ItemBasketRepository())
        {
        }

        public ItemBasketController(IItemBasketRepository itemBasketRepository)
        {
            this.itemBasketRepository = itemBasketRepository;
            this.database = Helper.GetDatabase();
        }

        // GET: Default
        public ActionResult ItemBasket(string itemId)
        {
            var viewModel = new ItemBasketViewModel();
            var item = this.database.GetItem(new ID(itemId));
            var itemBasketFolder = this.itemBasketRepository.GetBasketBucketItem(item);

            if (itemBasketFolder != null)
            {
                viewModel.LstBasketItems = this.itemBasketRepository.GetBucketItems(itemBasketFolder).ToList();
            }

            return this.View(viewModel);
        }

        public ActionResult RemoveItemFromBasket(string itemId)
        {
            var item = this.database.GetItem(itemId);
            var siteRoot = this.itemBasketRepository.GetSiteRoot(item);
            var itemBasketFolder = this.itemBasketRepository.GetBasketBucketItem(item);

            this.itemBasketRepository.RemoveItemFromBucket(itemBasketFolder, item);
            return this.RedirectToAction("ItemBasket", "ItemBasket", new { itemId = siteRoot.ID.ToString() });
        }

        [HttpPost]
        public ActionResult Operations()
        {
            var operation = this.Request.Params.AllKeys.Contains("ResetLayoutDetails") ? "ResetLayoutDetails" : this.Request.Params.AllKeys.Contains("PublishItems") ? "PublishItems" : this.Request.Params.AllKeys.Contains("MoveItemToRecycleBin") ? "MoveItemToRecycleBin" : string.Empty;
            Item siteroot = null;
            var idCollection = new IdCollection();
            if (!string.IsNullOrEmpty(operation))
            {

                try
                {
                    foreach (var key in this.Request.Params.AllKeys)
                    {
                        if (key.StartsWith("itemId"))
                        {
                            var id = key.Substring(7);
                            var itemId = ID.Parse(id);
                            var currentItem = Helper.GetItem(itemId);

                            //For setting current item while returning itembasket view
                            if (siteroot == null)
                                siteroot = this.itemBasketRepository.GetSiteRoot(currentItem);

                            using (new SecurityModel.SecurityDisabler())
                            {
                                if (operation == "PublishItems")
                                {
                                    idCollection.Add(itemId);
                                }
                                if (operation == "ResetLayoutDetails")
                                {
                                    using (new EditContext(currentItem))
                                    {
                                        currentItem?.Fields["__final renderings"].Reset();
                                    }
                                }
                                if (operation == "MoveItemToRecycleBin")
                                {
                                    using (new EditContext(currentItem))
                                    {
                                        var siteSettings = this.itemBasketRepository.GetSiteSettings(currentItem);
                                        var itemBasketFolder = siteSettings?.Children.FirstOrDefault(x => x.TemplateID.ToString() == Constants.TemplateIds.ItemBasketTemplateId);
                                        this.itemBasketRepository.RemoveItemFromBucket(itemBasketFolder, currentItem);

                                        currentItem?.Recycle();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Diagnostics.Log.Error("Error occured ItemBasketController:ResetLayoutDetails", ex, this);
                    return this.Content("Error occured while processing the items. Please try again.");
                }
            }
            if (operation == "PublishItems")
            {
                this.Session[Constants.IdCollSessionName] = idCollection;
                return this.RedirectToAction("GetPublishDetails", "Publish");
            }
            else
                return this.RedirectToAction("ItemBasket", "ItemBasket", new { itemId = siteroot?.ID.ToString() });
        }
    }
}