﻿namespace Sitecore.Feature.ItemBasket.Controllers
{
    using System.Web.Mvc;
    using System.Collections;
    using Glass.Mapper.Sc.Web.Mvc;
    using Sitecore.Collections;
    using Sitecore.Data;
    using Sitecore.Feature.ItemBasket.Models;
    using Sitecore.Feature.ItemBasket.Repository;
    using Sitecore.Globalization;

    public class PublishController : GlassController
    {
        private readonly IPublishRepository publishRepository;

        public PublishController():this (new PublishRepository())
        {
                
        }
        public PublishController(IPublishRepository publishRepository)
        {
            this.publishRepository = publishRepository;
        }
        // GET: Default
        public ActionResult GetPublishDetails()
        {
            var publishModel = this.publishRepository.GetPublishDetails();
            return this.View("PublishView", publishModel);
        }

        [HttpPost]
        public ActionResult Publish()
        {
            Language[] languages;
            Database[] databases;

            this.ParseValuesFromRequest(out languages, out databases);

            var idCollection = this.Session[Constants.IdCollSessionName] as IdCollection;

            var publishModel = new PublishModel
            {
                IsSubitems = !string.IsNullOrEmpty(this.Request["IsSubitems"]),
                IsRelatedItems = !string.IsNullOrEmpty(this.Request["IsRelatedItems"]),
                IsSmartPublish = (this.Request["IsSmartPublish"] == "smart"),
                LanguageCollection = languages,
                PublishingTargets = databases
            };
            this.publishRepository.Publish(publishModel, idCollection);
            return this.Content("Items have been added to publishing queue");
        }

        private void ParseValuesFromRequest(out Language[] languages, out Database[] databases)
        {
            var languageArray = new ArrayList();
            var databaseArray = new ArrayList();

            foreach (var item in this.Request.Params.AllKeys)
            {
                switch (item.Substring(0, 3))
                {
                    case "la_":
                        languageArray.Add(Language.Parse(item.Substring(3)));
                        break;
                    case "pb_":
                        databaseArray.Add(Configuration.Factory.GetDatabase(item.Substring(3)));
                        break;
                }
            }

            languages = (languageArray.ToArray(typeof(Language)) as Language[]);
            databases = (databaseArray.ToArray(typeof(Database)) as Database[]);
        }
    }
}