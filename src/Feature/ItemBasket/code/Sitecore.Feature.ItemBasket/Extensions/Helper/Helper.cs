﻿using System;
using Sitecore.Data.Items;
using Glass.Mapper.Sc;
using Sitecore.Data;

namespace Sitecore.Feature.ItemBasket.Extensions.Helper
{
    
    public static class Helper
    {
        /// <summary>
        /// Gets the database
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static Database GetDatabase(string dbName = "master")
        {
            return Database.GetDatabase(dbName);
        }

        /// <summary>
        /// Gets the sitecore Service
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static SitecoreService GetSitecoreService(string dbName = "master")
        {
            return new SitecoreService(dbName);
        }

        /// <summary>
        /// Gets Item by ID 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Item GetItem(string id)
        {
            return GetDatabase().GetItem(id);
        }

        /// <summary>
        /// Gets Item by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Item GetItem(ID id)
        {
            return GetDatabase().GetItem(id);
        }

        /// <summary>
        /// Gets a sitecore item cast as a specific Glass Type
        /// </summary>
        /// <typeparam name="T">Glass Type</typeparam>
        /// <param name="path">Path of the sitecore item</param>
        public static T GetItem<T>(string path) where T : class
        {
            return GetSitecoreService().GetItem<T>(path);
        }

        /// <summary>
        /// Gets a sitecore item cast as a specific Glass Type
        /// </summary>
        /// <typeparam name="T">Glass Type</typeparam>
        /// <param name="id">ID of the sitecore item</param>
        public static T GetItem<T>(Guid id) where T : class
        {
            return GetSitecoreService().GetItem<T>(id);
        }
    }
}