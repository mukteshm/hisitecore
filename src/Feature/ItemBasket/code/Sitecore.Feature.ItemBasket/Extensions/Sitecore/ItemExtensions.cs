﻿using System;
using Sitecore.Data.Items;
using Glass.Mapper.Sc;

namespace Sitecore.Feature.ItemBasket.Extensions.Sitecore
{
    

    public static class ItemExtensions
    {
        public static SitecoreService GetSitecoreService(string dbName = "master")
        {
            return new SitecoreService(dbName);
        }

        /// <summary>
        /// Casts a sitecore item as a specific Glass Type
        /// </summary>
        /// <typeparam name="T">Glass Type</typeparam>
        /// <param name="item">The Item to cast to a specific Type</param>
        public static T Cast<T>(this Item item) where T : class
        {
            return GetSitecoreService().Cast<T>(item);
        }

        public static Item GetFirstParentUsingTemplate(this Item currentItem, string baseTemplateNameOrId)
        {
            if (currentItem == null)
            {
                return null;
            }
            if (IsMatchOnTemplateNameOrId(baseTemplateNameOrId, currentItem.Template))
            {
                return currentItem;
            }
            var parent = currentItem.Parent;
            while (parent != null && IsMatchOnTemplateNameOrId(baseTemplateNameOrId, parent.Template) == false)
            {
                parent = parent.Parent;
            }
            return parent;
        }
        private static bool IsMatchOnTemplateNameOrId(string baseTemplateNameOrId, TemplateItem baseTemplateItem)
        {
            return baseTemplateItem.ID.Guid.ToString("B").Equals(baseTemplateNameOrId, StringComparison.OrdinalIgnoreCase) ||
                                baseTemplateItem.FullName.Equals(baseTemplateNameOrId, StringComparison.OrdinalIgnoreCase) ||
                                baseTemplateItem.Name.Equals(baseTemplateNameOrId, StringComparison.OrdinalIgnoreCase);
        }
    }
}