﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Feature.ItemBasket.Models
{
    public class ItemBasketViewModel : GlassBase
    {
        public List<_Item_Detail> LstBasketItems
        {
            get; set;
        }
    }
}