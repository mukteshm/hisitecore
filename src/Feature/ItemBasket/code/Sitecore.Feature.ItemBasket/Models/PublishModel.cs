﻿namespace Sitecore.Feature.ItemBasket.Models
{
    using Sitecore.Data;
    using Sitecore.Globalization;

    public class PublishModel
    {
        public bool IsSmartPublish { get; set; }
        public Language[] LanguageCollection { get; set; }
        public Database[] PublishingTargets { get; set; }
        public bool IsSubitems { get; set; }
        public bool IsRelatedItems { get; set; }
    }
}