﻿using System.Collections.Generic;

namespace Sitecore.Feature.ItemBasket.Repository
{
    using Sitecore.Data.Items;

    public interface IItemBasketRepository
    {
        Item GetSiteSettings(Item item);
        List<_Item_Detail> GetBucketItems(Item itemBasketFolder);
        Item GetSiteRoot(Item item);
        bool RemoveItemFromBucket(Item itemBasketFolder, Item item);
        Item GetBasketBucketItem(Item item);
    }
}