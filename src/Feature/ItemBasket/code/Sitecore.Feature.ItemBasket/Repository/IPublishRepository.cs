﻿
namespace Sitecore.Feature.ItemBasket.Repository
{
    using Sitecore.Collections;
    using Sitecore.Feature.ItemBasket.Models;

    public interface IPublishRepository
    {
        PublishModel GetPublishDetails();
        int Publish(PublishModel publishModel, IdCollection idCollection);
    }
}