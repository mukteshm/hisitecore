﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.Buckets.Managers;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Feature.ItemBasket.Extensions.Helper;
using Sitecore.Feature.ItemBasket.Extensions.Sitecore;

namespace Sitecore.Feature.ItemBasket.Repository
{
    public class ItemBasketRepository : IItemBasketRepository
    {
        #region Methods
        public Item GetSiteRoot(Item item)
        {
           return item.GetFirstParentUsingTemplate(Constants.TemplateIds.SiteRootTempalteId);
        }
        public Item GetSiteSettings(Item item)
        {
            //TODO:Use Multisite foundation code
            var root = item.GetFirstParentUsingTemplate(Constants.TemplateIds.SiteRootTempalteId);
            return root?.Children.FirstOrDefault(x => x.TemplateID.ToString() == Constants.TemplateIds.SiteSettingTempalteId);
        }
        #endregion

        /// <summary>
        /// Check if item exists in buckets.
        /// </summary>
        /// <param name="bucketItem"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool CheckifItemExistsInBasket(Item bucketItem, Item item)
        {
            if (bucketItem == null || !BucketManager.IsBucket(bucketItem) || item == null)
            {
                return false;
            }
            var itemId = item.ID.ToString();
            using (var searchContext = ContentSearchManager.GetIndex(Constants.Databases.MasterDatabase).CreateSearchContext())
            {
                var result = searchContext.GetQueryable<_Item_Detail>().FirstOrDefault(x => x.Item_ID == itemId);
                if (result != null)
                    return true;

            }
            return false;
        }

        /// <summary>
        /// Get bucket items
        /// </summary>
        /// <param name="bucketItem"></param>
        /// <returns></returns>
        public List<_Item_Detail> GetBucketItems(Item bucketItem)
        {
            if (bucketItem == null || !BucketManager.IsBucket(bucketItem))
            {
                return null;
            }
            using (var searchContext = ContentSearchManager.GetIndex("sitecore_master_index").CreateSearchContext())
            {
                //TODO:Use feature interface template for filtering
                var results = searchContext.GetQueryable<SearchResultItem>().Where(x => x.TemplateId == new ID(Constants.TemplateIds.ItemTemplateId));
                return this.ProcessSearchResult(results);

            }
        }

        /// <summary>
        /// Process Search results
        /// </summary>
        /// <param name="searchItems"></param>
        /// <returns></returns>
        public List<_Item_Detail> ProcessSearchResult(IEnumerable<SearchResultItem> searchItems)
        {
            var items = new List<_Item_Detail>();
            foreach (var searchItem in searchItems)
            {
                //TODO:Check if below code works
                //Helper.GetSitecoreService().GetItem<_Item_Detail>(searchItem.ItemId.ToString());
                var item = Helper.GetDatabase().GetItem(searchItem.ItemId);
                if(item != null && item.Name.ToLower() != "__standard values")
                    items.Add(item.Cast<_Item_Detail>());
            }

            return items;
        }

        /// <summary>
        /// Removes item from Item Basket
        /// </summary>
        /// <param name="bucketItem"></param>
        /// <param name="itemToRemove"></param>
        /// <returns></returns>
        public bool RemoveItemFromBucket(Item bucketItem, Item itemToRemove)
        {
            //check if item exists
            if (!this.CheckifItemExistsInBasket(bucketItem, itemToRemove))
            {
                return false;
            }
            var itemToRemoveFromBucket = this.GetBucketItems(bucketItem).FirstOrDefault(x => x.Item_ID == itemToRemove.ID.ToString());
            Helper.GetSitecoreService().Delete(itemToRemoveFromBucket);
            
            return true;
        }

        public Item GetBasketBucketItem(Item item)
        {
            if (item == null) return null;

            var siteSettings = this.GetSiteSettings(item);
            var itemBasketFolder = siteSettings?.Children.FirstOrDefault(x => x.TemplateID.ToString() == Constants.TemplateIds.ItemBasketTemplateId);
            return itemBasketFolder;
        }
    }
}