﻿
namespace Sitecore.Feature.ItemBasket.Repository
{
    using System.Collections;
    using System.Linq;
    using Sitecore.Collections;
    using Sitecore.Data;
    using Sitecore.Data.Managers;
    using Sitecore.Feature.ItemBasket.Extensions.Helper;
    using Sitecore.Feature.ItemBasket.Models;
    using Sitecore.Publishing;

    public class PublishRepository : IPublishRepository
    {
        public PublishModel GetPublishDetails()
        {
            var databaseArray = new ArrayList();
            foreach (var item in PublishManager.GetPublishingTargets(Context.Database))
            {
                databaseArray.Add(Configuration.Factory.GetDatabase(item["Target database"]));
            }
            var publishModel = new PublishModel
            {
                LanguageCollection = LanguageManager.GetLanguages(Context.Database).ToArray(),
                PublishingTargets = databaseArray.ToArray(typeof(Database)) as Database[]

            };
            return publishModel;
        }

        public int Publish(PublishModel publishModel, IdCollection idCollection)
        {
            
            if (idCollection != null)
            {
                foreach (var item in idCollection.Select(Helper.GetItem))
                {
                    //TODO: Revise publishing logic to include reference item at the time of additemreference pipeline - so that publishing can be done in single job
                    PublishManager.PublishItem(item, publishModel.PublishingTargets, publishModel.LanguageCollection, publishModel.IsSubitems, publishModel.IsSmartPublish,publishModel.IsRelatedItems);
                }
            }
            return 1;
        }
    }
}